package com.android.showhi.showhibeta.mainflow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.showhi.showhibeta.R;

/**
 * Created by littleflyer on 2017/12/20.
 */


public class TemplateFragment extends BaseFragment {

    private String TAG = this.getClass().getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.template_fragment, container,
                false);
        TextView textView = (TextView) view.findViewById(R.id.text);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        return view;
    }


    public void handleMessage() {

        Bundle args = getArguments();
        if (args != null) {

        }
    }

    public static TemplateFragment newInstance(int position) {
        TemplateFragment f = new TemplateFragment();
        return f;
    }
}
