package com.android.showhi.showhibeta.mainflow;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.showhi.showhibeta.R;
import com.android.showhi.showhibeta.server.RequestApi;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okio.ByteString;

/**
 * Created by littleflyer on 2017/12/20.
 */

public class LoginActivity extends AppCompatActivity {


    private Button confirm;
    private Button cancel;
    private EditText account;
    private EditText password;
    private String TAG = this.getClass().getSimpleName();
    private ProgressDialog dialog;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        confirm = findViewById(R.id.confirm);
        cancel = findViewById(R.id.cancel);
        account = findViewById(R.id.account);
        password = findViewById(R.id.password);


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = ProgressDialog.show(LoginActivity.this,
                        getString(R.string.login_string1), getString(R.string.login_string2), true);
                Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case 0:
                            case 1:
                            case 2:
                                Toast.makeText(LoginActivity.this, getString(R.string.login_string4), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                                break;
                            case 100:
                                bundle = msg.getData();
                                startActivity();
                                dialog.dismiss();
                                break;
                            default:
                                Toast.makeText(LoginActivity.this, getString(R.string.login_string1), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                                break;
                        }
                    }
                };
                RequestApi requestApi = new RequestApi(handler);
                requestApi.loginIn(account.getText().toString(), password.getText().toString());
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                account.setText("");
                password.setText("");
            }
        });
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i(TAG, "applicationId: " + getPackageName() +
                        "\nSHA-1 certificate signature: " + ByteString.of(md.digest()).hex());
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i(TAG, "" + e);
        } catch (NoSuchAlgorithmException e) {
            Log.i(TAG, "" + e);
        }
    }

    private void startActivity() {

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtras(bundle);
        LoginActivity.this.startActivity(intent);
        dialog.dismiss();
        finish();

    }
}
