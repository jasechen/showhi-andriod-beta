package com.android.showhi.showhibeta.server;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by littleflyer on 2017/12/20.
 */


public class RequestApi {


    public static String domainName = "https://showhi.co/forend/Public/showhi/?service=";
    public static String domainName2 = "https://api.showhi.co/Public/showhi/?service=";
    private Handler handler;
    private String TAG = this.getClass().getSimpleName();
    private Map<String, String> map = new HashMap<String, String>();
    private Message msg = new Message();
    private Bundle bundle = new Bundle();

    public RequestApi(Handler handler) {
        this.handler = handler;
    }


    public void loginIn(String loginid, String loginpwd) {

        String url = domainName2 + "User.logIn";
        try {
            OkHttpClient okHttpClient = new OkHttpClient();
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("identity", "teach");
            builder.addFormDataPart("loginpwd", loginpwd);
            builder.addFormDataPart("loginid", loginid);
            builder.addFormDataPart("time_set", "9");
            RequestBody body = builder.build();
            final Request request = new Request.Builder().url(url).post(body).header("api_key", "TEIJFIDM3569").build();
            final Call call = okHttpClient.newBuilder().writeTimeout(50, TimeUnit.SECONDS).build().newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    bundle.putString("errorMsg", e.toString());
                    msg.setData(bundle);
                    msg.what = 0;
                    handler.sendMessage(msg);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String string = response.body().string();
                        Log.i(TAG, "response ----->" + string);
                        msg.what = 100;
                        handler.sendMessage(msg);
                    } else {
                        bundle.putString("errorMsg", "okhttp get response error");
                        msg.setData(bundle);
                        msg.what = 1;
                        handler.sendMessage(msg);
                    }
                }
            });
        } catch (Exception e) {
            bundle.putString("errorMsg", e.toString());
            msg.setData(bundle);
            msg.what = 2;
            handler.sendMessage(msg);
        }
    }
}
