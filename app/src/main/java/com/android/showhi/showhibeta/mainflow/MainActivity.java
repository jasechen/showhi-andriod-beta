package com.android.showhi.showhibeta.mainflow;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration;
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager;
import com.android.showhi.showhibeta.aws.PushListenerService;
import com.android.showhi.showhibeta.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * Created by littleflyer on 2017/12/20.
 */

public class MainActivity extends AppCompatActivity implements BaseFragment.communicateWithMainActivity, android.app.FragmentManager.OnBackStackChangedListener {

    private TextView mTextMessage;
    public static PinpointManager pinpointManager;
    private FragmentManager manager;
    private String TAG = this.getClass().getSimpleName();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;


        }
    };


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initPinPoint();
        init();
    }

    private void init() {

        manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        WallFragment wallFragment = new WallFragment();
        sendMessage(wallFragment, getIntent().getExtras());
        transaction.add(R.id.container,
                wallFragment,
                wallFragment.getClass().getSimpleName());
        transaction.commit();
        manager.addOnBackStackChangedListener(this);


    }

    private void initPinPoint() {
        AWSMobileClient.getInstance().initialize(this).execute();
        if (pinpointManager == null) {
            PinpointConfiguration pinpointConfig = new PinpointConfiguration(
                    getApplicationContext(),
                    AWSMobileClient.getInstance().getCredentialsProvider(),
                    AWSMobileClient.getInstance().getConfiguration());

            pinpointManager = new PinpointManager(pinpointConfig);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String deviceToken =
                                InstanceID.getInstance(MainActivity.this).getToken(
                                        "819528590192",
                                        GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                        Log.i(TAG, "the deviceToken is" + deviceToken);
                        pinpointManager.getNotificationClient()
                                .registerGCMDeviceToken(deviceToken);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // unregister notification receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register notification receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(notificationReceiver,
                new IntentFilter(PushListenerService.ACTION_PUSH_NOTIFICATION));
    }

    private final BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "Received notification from local broadcast. Display it in a dialog.");

            Bundle data = intent.getBundleExtra(PushListenerService.INTENT_SNS_NOTIFICATION_DATA);
            String message = PushListenerService.getMessage(data);

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Push notification")
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }
    };

    @Override
    public void createFragment(String fragmentPath, Bundle bundle) {

        FragmentTransaction transaction = manager.beginTransaction();
        Fragment currentFragment = manager.findFragmentById(R.id.container);
        transaction.hide(currentFragment);
        //Log.i(TAG, "fragmentPath " + fragmentPath);
        try {
            Class<?> object = Class.forName(fragmentPath);
            Fragment fragment = (Fragment) object.newInstance();
            if (fragment != null) {
                transaction.add(R.id.container, fragment);
                sendMessage(fragment, bundle);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
                //Log.i(TAG, "createFragment" + fragmentPath);
            }
        } catch (Exception e) {
            Log.i(TAG, "createFragment fail" + e);

        }

    }

    public void sendMessage(Fragment fragment, Bundle msg) {

        fragment.setArguments(msg);

    }


    @Override
    public void createActivity(String activityPath) {

    }


    @Override
    public void onBackStackChanged() {
        String screenName;
        FragmentManager manager = getFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.container);
        int pageNum = manager.getBackStackEntryCount();
        Log.d(TAG, "第" + pageNum + "頁 " + fragment.getClass().getSimpleName());

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment = manager.findFragmentById(R.id.container);
        if (fragment != null) {
            if (fragment.isHidden() == true) {
                transaction.show(fragment).commit();
            }
        }

    }

}
