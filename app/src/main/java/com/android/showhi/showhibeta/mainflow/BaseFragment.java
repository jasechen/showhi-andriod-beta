package com.android.showhi.showhibeta.mainflow;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

/**
 * Created by littleflyer on 2017/12/20.
 */

public class BaseFragment extends Fragment implements View.OnClickListener {
    public communicateWithMainActivity callback;


    public interface communicateWithMainActivity {
        public void createFragment(String fragmentPath , Bundle bundle);
        public void createActivity(String activityPath);
    }

    public void hideSelf() {
        hideSelfAllowingStateLoss();
    }

    private void hideSelfAllowingStateLoss() {
        FragmentManager manager = getActivity().getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.hide(this);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleMessage();
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            if (activity.getClass().getSimpleName().equals("MainActivity")
                    ) {
                callback = (communicateWithMainActivity) activity;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement communicateWithMainActivity");
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
        } else {// show
            handleMessage();
        }
    }

    public void handleMessage() {

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }
}